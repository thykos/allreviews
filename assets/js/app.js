$(document).ready(function() {
  $('#main-slider').owlCarousel({
    items: 3,
    navigation: true,
    responsive: false,
    pagination: false,
    navigationText: ['','']
  });

  $('#second-slider').owlCarousel({
    items: 8,
    navigation: true,
    responsive: false,
    pagination: false,
    navigationText: ['','']
  });

  $('.paginavi .page').on('click', function() {
    $('.paginavi .page').removeClass('active');
    $(this).addClass('active');
  })
});